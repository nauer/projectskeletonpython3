import datetime

temp = """<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Sophisticated Results</title>
</head>
<body>
<h1><i>{timestamp}</i> - <b>{message}</b></h1>
</body>
</html>"""

def report_text(message, file_stream):
    """
    Schreibt Message mit Timestamp im simplen Text-Format in file_stream
    :param message:
    :param file_stream:
    :return: None
    """
    print("{timestamp} - {message}".format(timestamp=datetime.datetime.now(), message=message), file=file_stream)


def report_html(message, file_stream):
    """
    Schreibt Message mit Timestamp im simplen HTML-Format in file_stream
    :param message:
    :param file_stream:
    :return: None
    """
    print(temp.format(timestamp=datetime.datetime.now(), message=message), file=file_stream)


if __name__ == "__main__":
    import sys

    report_text("Welcome to package reports!", file_stream=sys.stdout)
    report_html("Welcome to package reports!", file_stream=sys.stdout)
