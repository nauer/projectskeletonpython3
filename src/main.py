import argparse
import sys

from src.simplemath.operators import divide, multiply
from src.reports.reports_text import report_text, report_html

"""
Programm Beschreibung
"""

DEBUG = False

__version__ = "0.2"


# Main Function
def start(args):
    if args.report == "TEXT":
        print(report_text(args.operators(*args.integers), args.output))
    elif args.report == "HTML":
        print(report_html(args.operators(*args.integers), args.output))


def main():
    # Argparse Section
    parser = argparse.ArgumentParser(description="Dividiert oder multipliziert Werte (*, /)")
    parser.add_argument("-v", "--version", action="version", version=__version__)

    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument(
        "-m", "--multiply", action="store_const", const=multiply, dest="operators", help="Werte werden multipliziert"
    )
    group.add_argument(
        "-d", "--divide", action="store_const", const=divide, dest="operators", help="Werte werden dividiert"
    )

    parser.add_argument("-r", "--report", choices=("HTML", "TEXT"), default="TEXT", help="Output Format. Default:'TEXT'")
    parser.add_argument("integers", metavar="N", type=int, nargs=2, help="Integer Wert")
    parser.add_argument(
        "-o",
        "--output",
        metavar="OUTPUT",
        type=argparse.FileType("w"),
        default=sys.stdout,
        help="Output Filename. Default: 'Stdout'",
    )
    parser.set_defaults(func=start)


    if DEBUG:
        # args = parser.parse_args(['-h'])
        # args = parser.parse_args(['-v'])
        args = parser.parse_args(["-m", "-r", "HTML", "9", "5"])
        # args = parser.parse_args(['-d', '-r', 'TEXT', '9', '5'])

        print(args)
    else:
        args = parser.parse_args()

    return start(args)

if __name__ == "__main__":
    # Start program
    main()
