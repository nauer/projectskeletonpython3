def divide(x, y):
    """
    Dividiert x durch y
    :param x: numeric
    :param y: numeric
    :return: numeric
    """
    return x / y


def multiply(x, y):
    """
    Multipliziert x mit y
    :param x: numeric
    :param y: numeric
    :return: numeric
    """
    return x * y


if __name__ == "__main__":
    print(divide(5, 2))
    print(multiply(5, 2))
