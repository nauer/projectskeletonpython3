from reports import reports_text as r_txt
from reports import reports_pdf as r_pdf
import datetime
from io import StringIO

"""
    Unittesting mit pytest
    https://docs.pytest.org/en/latest/

    Alle Funktionen die getestet werden sollen müssen mit 'test' beginnen 
"""

FAKE_TIME = "2018-03-17 16:54:53.638425"

f = StringIO()

# Fake Klasse für datetime
class mydatetime:
    @classmethod
    def now(cls):
        return FAKE_TIME

# Ersetzt datetime.datetime.now() Rückgabe mit FAKE_TIME
# https://docs.pytest.org/en/latest/monkeypatch.html
def test_report(monkeypatch):
    monkeypatch.setattr(datetime, "datetime", mydatetime)

    r_txt.report_text("Hello\nWorld", f)

    f.seek(0)
    result = f.read()
    test = "{} - Hello\nWorld\n".format(datetime.datetime.now())

    assert result == test
